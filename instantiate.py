#!/usr/bin/env python3

import argparse
import json
import os
import subprocess
import shutil

## global constants

CONFIG_DEFAULTS = {
    'name': os.path.basename(os.path.dirname(os.path.abspath(__file__))),
    'description': 'my first package',
    'version': '0.0.1',
    'install_requires': [],
}

GIT_IGNORE_OPTIONAL_SECTIONS = {
    'syncthing': """
*.sync-conflict-*
""",
    'emacs': """
auto/
"""
}

EXAMPLE_MODULE = 'mymodule'
EXAMPLE_FUNCTION = 'myfunction'

PATH_GITIGNORE = '.gitignore'
PATH_INSTANTIATION_PARAMETERS = 'template_parameters.json'
PATH_PYLINTRC = 'pylintrc'
PATH_README = 'README.md'
PATH_SELF = 'instantiate.py'
PATH_SETUP_CFG = 'setup.cfg'
PATH_TESTS_ROOT = 'tests'

## helpers

def touch(path):
    """Like gnu `touch`."""
    with open(path, 'a'):
        pass

def git_checkout(*paths):
    """Call `git checkout -- ...`"""
    subprocess.run(['git', 'checkout', '--', *paths])

## business logic

def cleanup():
    try:
        config = get_instantiation_config(interactive=False)
    except OSError:
        print('cleanup: nothing to do')
        return
    for path in [PATH_SETUP_CFG,
                 # PATH_INSTANTIATION_PARAMETERS,
                 PATH_README,
                 ]:
        os.unlink(path)
    for path in [config['name'] + '.egg-info',
                 config['name'],
                 PATH_TESTS_ROOT,
                 ]:
        try:
            shutil.rmtree(path)
        except OSError:
            pass
    git_checkout(PATH_README, PATH_GITIGNORE)

def get_instantiation_config(interactive=True):
    try:
        with open(PATH_INSTANTIATION_PARAMETERS) as file:
            config = json.load(file)
            return config
    except OSError as e:
        if not interactive:
            raise e
    config = {}
    for key, default in CONFIG_DEFAULTS.items():
        if isinstance(default, str):
            value = input(f'name [default="{default}"] = ')
            if not value:
                value = default
        elif isinstance(default, list):
            value = []
            while True:
                item = input(f'add {key} (one per line, end with empty line): ')
                if item:
                    value.append(item)
                else:
                    break
        config[key] = value
    return config

def patch_gitignore(config):
    try:
        sections = config['gitignore_sections']
    except KeyError:
        keys = list(GIT_IGNORE_OPTIONAL_SECTIONS)
        for i, key in enumerate(keys):
            print(f'{i:2d}: {key}')
        selection = input('select additional `.gitignore`\n'
                          'sections[space-seperated numbers, e.g. "1 2", '
                          'or "a" for all] ')
        if selection == 'a':
            selection = range(len(keys))
        else:
            selection = list(map(int, selection.split()))
        sections = [keys[i] for i in selection]
        config['gitignore_sections'] = sections
    if sections:
        with open(PATH_GITIGNORE, 'a') as file:
            for sec in sections:
                print(f'\n# {sec}', end='', file=file)
                print(GIT_IGNORE_OPTIONAL_SECTIONS[sec], file=file)


def write_config_files(config):
    ## README.md
    name                = config['name']
    description         = config['description']
    version             = config['version']
    install_requires    = config['install_requires']
    with open(PATH_README, 'w') as file:
        print(f'# {name}\n\n{description}', file=file)

    ## setup.cfg
    with open(PATH_SETUP_CFG, 'w') as file:
        print(f"""
[metadata]
name = {name}
version = {version}

[options]
packages = {name}""", file=file)
        if install_requires:
            print('install_requires =', file=file)
            for x in install_requires:
                print(f'    {x}', file=file)
        print(f"""
setup_requires =
    pytest-runner
    setuptools-lint

tests_require =
    pytest

[aliases]
test=pytest

[lint]
lint_rcfile={PATH_PYLINTRC}
lint_packages=
    {name}
    tests
""", file=file)
    ## package root directory
    try:
        os.mkdir(name)
    except FileExistsError:
        pass
    touch(os.path.join(name, '__init__.py'))
    ## .gitignore
    patch_gitignore(config)
    ## example code
    write_example_code(config)
    ## dump config
    with open(PATH_INSTANTIATION_PARAMETERS, 'w') as file:
        json.dump(config, file, indent=2)
        print('', file=file) # add a newline

def write_example_code(config):
    name = config['name']
    ## source code
    with open(os.path.join(name, f'{EXAMPLE_MODULE}.py'), 'w') as file:
        print(f"""\"\"\"Module docstring of {name}.{EXAMPLE_MODULE}

Lorem ipsum.
\"\"\"

def {EXAMPLE_FUNCTION}(x):
    \"\"\"Add 23 to first argument.\"\"\"
    return x + 23""", file=file)
    ## test code
    try:
        os.mkdir(PATH_TESTS_ROOT)
    except FileExistsError:
        pass
    with open(os.path.join(PATH_TESTS_ROOT, f'test_{EXAMPLE_MODULE}.py'), 'w') as file:
        print(f"""\"\"\"Module docstring of test_{EXAMPLE_MODULE}

Lorem ipsum.
\"\"\"

import time

import pytest

from {name}.{EXAMPLE_MODULE} import {EXAMPLE_FUNCTION}

def test_pass():
    \"\"\"Test that passes.\"\"\"
    assert True

def test_fail():
    \"\"\"Test that fails.\"\"\"
    assert False

def test_{EXAMPLE_FUNCTION}():
    \"\"\"Test of `{EXAMPLE_FUNCTION}()`.\"\"\"
    assert {EXAMPLE_FUNCTION}(17) == 40

@pytest.mark.slow
def test_fail_slow():
    \"\"\"Really slow test that passes.\"\"\"
    time.sleep(1)
    assert True""", file=file)

def reinit_git_remove_script():
    p_git_remote = subprocess.run(['git', 'remote', 'get-url', 'origin'],
                                  stdout=subprocess.PIPE, text=True)
    if p_git_remote.returncode != 0:
        print('ABORTED')
        return
    upstream_url = p_git_remote.stdout
    if input(f'about to `rm -rf .git` with origin "{upstream_url}"\n'
             'type "yes" to proceed: ') != 'yes':
        print('ABORTED')
        return
    shutil.rmtree('.git')
    os.unlink(PATH_SELF)
    print('I removed myself (rm ./instantiate.py)')
    print(f'Dont forget to `rm {PATH_INSTANTIATION_PARAMETERS}`.')
    subprocess.run(['git', 'init'])
    subprocess.run(['git', 'add', '-A'])
    subprocess.run(['git', 'reset', PATH_INSTANTIATION_PARAMETERS])
    print("""
You may now use the following commands:

./setup.py lint # lint your code
./setup.py test # test your code
[any regular setup.py commands]
    see https://setuptools.pypa.io/en/latest/userguide/commands.html]

Happy hacking!
""")


if __name__=='__main__':
    parser = argparse.ArgumentParser(description='Instantiate python project template.')
    parser.add_argument('-c', '--clean', action='store_true')
    parser.add_argument('-f', '--finish', action='store_true')
    args = parser.parse_args()
    if args.clean:
        cleanup()
        exit(0)
    if args.finish:
        reinit_git_remove_script()
        exit(0)
    config = get_instantiation_config()
    write_config_files(config)
    print()
    print('To undo instantiation, run')
    print('  ./instantiate.py -c')
    print('To finalize instantiation, run')
    print('  ./instantiate.py -f')
