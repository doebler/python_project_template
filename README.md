# A Template for Distributable Python Projects

This git repo contains all the boilerplate stuff of a `setuptools`-enabled
python project.

## Components Included

This template includes the code you get, if you copy&paste stuff from the
following howtows:

- [setuptools](https://setuptools.pypa.io/en/latest/userguide/quickstart.html)
  - directory structure skeleton
  - `setup.py`
  - `setup.cfg`
- git boilerplate
  - `.gitignore`
  - `README.md` (at least in gitlab/github)
- [pytest](https://docs.pytest.org/)
  - [pytest config skeleton](https://docs.pytest.org/en/6.2.x/example/simple.html#control-skipping-of-tests-according-to-command-line-option)
    - `conftest.py`
  - [pytest integration for setuptools](https://godatadriven.com/blog/a-practical-guide-to-using-setup-py/)
- a script to instantiate the template

## Basic Usage
```sh
git clone https://gitlab.com/doebler/python_project_template.git my_cool_project # clone the template
cd my_cool_project
./instantiate.py    # set name, description, dependencies, ...
./instantiate.py -f # break ties with this repo, initialize a new one
                    # and remove unneeded files
```

Author: Holger Döbler <holger.doebler@posteo.de>
